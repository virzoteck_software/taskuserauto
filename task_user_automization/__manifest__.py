# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'User Assignment to Stage',
    'version': '2.0',
    'category': 'Project',
    'complexity': 'easy',
    'website': '',
    'description': """This module allows to auto user assignmet in task as par task stage.This module Works on both community and enterprise version 11""",
    'depends': ['project'],
    'data': [
        'views/project_view.xml',

    ],
    'installable': True,
    'auto_install': True,
    'images': ['static/description/background.jpg',],
    'license': 'LGPL-3',
    "price":12,
    "currency": "EUR"
}