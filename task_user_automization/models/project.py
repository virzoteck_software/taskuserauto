# -*- coding: utf-8 -*-

from odoo import api, models, fields


class TaskAutomation(models.Model):
    _name = 'task.automation'

    @api.multi
    def _get_default_project_id(self):
        print ('vals========%s' % self._context.get('project_id'))
        return self.env.context.get('project_id')

    task_auto_user_id = fields.Many2one('project.project', string='User Auto Assignment',
                                        default=_get_default_project_id)
    user_id = fields.Many2one('res.users', string='Assigned to user', required=True)
    task_type = fields.Many2one('project.task.type', string='Project Task Stage', required=True)


class ProjectProject(models.Model):
    _inherit = 'project.project'

    task_auto_user_ids = fields.One2many('task.automation', 'task_auto_user_id', 'User Auto Assignment')
