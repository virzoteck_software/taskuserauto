# -*- coding: utf-8 -*-

from odoo import api, models, fields


class ProjectTask(models.Model):
    _inherit = 'project.task'


    @api.model
    def create(self, vals):
        task_automation_obj = self.env['task.automation']
        project_obj = self.env['project.task']

        if vals.get('project_id', False):
            task_auto_id = task_automation_obj.search(
                [('task_auto_user_id', '=', vals['project_id']), ('task_type', '=', vals['stage_id'])])
            if len(task_auto_id):
                vals['user_id'] = task_auto_id[0].user_id.id

        return super(ProjectTask, self).create(vals)

    @api.multi
    def write(self, vals):
        now = fields.Datetime.now()
        task_automation_obj = self.env['task.automation']
        # stage change: update date_last_stage_update
        if 'stage_id' in vals:
            vals.update(self.update_date_end(vals['stage_id']))
            task_auto_id = task_automation_obj.search(
                [('task_auto_user_id', '=', self.project_id.id), ('task_type', '=', vals['stage_id'])])
            if len(task_auto_id):
                # print ('vals==99999999999999999999999999999======%s' % task_auto_id)
                # print ('vals==00000000rrrr======%s' % self.project_id.name)
                # print ('vals==00000000======%s' % self.stage_id.name)

                vals['user_id'] = task_auto_id.user_id.id

            vals['date_last_stage_update'] = now

            # reset kanban state when changing stage
            if 'kanban_state' not in vals:
                vals['kanban_state'] = 'normal'
        # user_id change: update date_assign
        if vals.get('user_id') and 'date_assign' not in vals:
            vals['date_assign'] = now

        return super(ProjectTask, self).write(vals)

